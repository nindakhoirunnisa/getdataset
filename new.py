from selenium import webdriver
import pandas as pd
from webdriver_manager.chrome import ChromeDriverManager
import itertools
import time
import MySQLdb

HOST = "localhost"
USERNAME = "scraping_user"
PASSWORD = ""
DATABASE = "symptoms_diagnose_dataset"

df = pd.read_pickle('C:/Users/HP/Documents/IMA/getdataset/input_clean.pkl')
driver = webdriver.Chrome(ChromeDriverManager().install())
driver.get("https://www.mayoclinic.org/symptom-checker/abdominal-pain-in-adults-adult/related-factors/itt-20009075")

def get_data():
    Find = driver.find_element_by_xpath('//*[@id="FindCause"]')
    driver.execute_script("arguments[0].click();", Find)

    satu = driver.find_element_by_xpath('//*[@id="main_0_maincontent_1_RepeaterCause_causeDiv_0"]/h3/a').text
    dua = driver.find_element_by_xpath('//*[@id="main_0_maincontent_1_RepeaterCause_causeDiv_1"]/h3/a').text
    tiga = driver.find_element_by_xpath('//*[@id="main_0_maincontent_1_RepeaterCause_causeDiv_2"]/h3/a').text

    cursor.execute("INSERT INTO gejala_diagnosa (pain_is, pain_located_in, triggered_by, relieved_by, accompanied_by, diagnosa_1, diagnosa_2, diagnosa_3) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)", (['; '.join(pain_is)], ['; '.join(pain_located_in)], ['; '.join(triggered_by)], ['; '.join(relieved_by)], ['; '.join(accompanied_by)], satu, dua, tiga))
    db.commit()

    #Back = driver.find_element_by_xpath('//*[@id="main_0_maincontent_1_LinkButton2"]')
    #driver.execute_script("arguments[0].click();", Back)
    driver.get("https://www.mayoclinic.org/symptom-checker/abdominal-pain-in-adults-adult/related-factors/itt-20009075")

correct = False
while not correct:
    try:
        batasbawah = int(input("Mulai dari data ke: "))
        batasatas = int(input("Sampai data ke: "))
        correct = True
    except:
        print("Masukkan angka.")

else:
    for barisnomor in range((batasbawah-1), batasatas):
        pain_is = []
        pain_located_in = []
        triggered_by = []
        relieved_by = []
        accompanied_by = []
        
        p_painis = df.iloc[barisnomor][0]
        p_lokasi = df.iloc[barisnomor][1]
        p_trigger = df.iloc[barisnomor][2]
        p_reli = df.iloc[barisnomor][3]
        p_acc = df.iloc[barisnomor][4]

        tupel1 = tuple([i for i, j in enumerate(p_painis) if j == 1])
        tupel2 = tuple([i for i, j in enumerate(p_lokasi) if j == 1])
        tupel3 = tuple([i for i, j in enumerate(p_trigger) if j == 1])
        tupel4 = tuple([i for i, j in enumerate(p_reli) if j == 1])
        tupel5 = tuple([i for i, j in enumerate(p_acc) if j == 1])

        db = MySQLdb.connect(HOST, USERNAME, PASSWORD, DATABASE)
        cursor = db.cursor()

        for angka in tupel1:
            pain_click = driver.find_elements_by_class_name("frm_options")[0].find_elements_by_tag_name("li")[angka]
            pain_click.click()
            pain_is_text = driver.find_elements_by_class_name("frm_options")[0].find_elements_by_tag_name("label")[angka].text
            pain_is.append(pain_is_text)
            
        for angka in tupel2:
            painloc_click = driver.find_elements_by_class_name("frm_options")[1].find_elements_by_tag_name("li")[angka]
            painloc_click.click()
            pain_located_in_text = driver.find_elements_by_class_name("frm_options")[1].find_elements_by_tag_name("label")[angka].text
            pain_located_in.append(pain_located_in_text)
                
        for angka in tupel3:
            trigger_click = driver.find_elements_by_class_name("frm_options")[2].find_elements_by_tag_name("li")[angka]
            trigger_click.click()
            triggered_by_text = driver.find_elements_by_class_name("frm_options")[2].find_elements_by_tag_name("label")[angka].text
            triggered_by.append(triggered_by_text)
                    
        for angka in tupel4:
            reliever_click = driver.find_elements_by_class_name("frm_options")[3].find_elements_by_tag_name("Li")[angka]
            reliever_click.click()
            relieved_by_text = driver.find_elements_by_class_name("frm_options")[3].find_elements_by_tag_name("label")[angka].text
            relieved_by.append(relieved_by_text)
                        
        for angka in tupel5:
            accompanier_click = driver.find_elements_by_class_name("frm_options")[4].find_elements_by_tag_name("input")[angka]
            accompanier_click.click()
            accompanied_by_text = driver.find_elements_by_class_name("frm_options")[4].find_elements_by_tag_name("label")[angka].text
            accompanied_by.append(accompanied_by_text)

        get_data()
    cursor.close()